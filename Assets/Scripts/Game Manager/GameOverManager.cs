﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour {

	public static GameOverManager instance;
	private GameObject gameOverPanel;
	private Animator gameOverAnim;
	private GameObject scoreText;
	private Text finalScore;
	private Button RestartBtn, BackBtn;

	void Awake(){
		MakeInstance ();
		InitializeVariables ();
	}

	void MakeInstance(){

		if (instance == null)
			instance = this;
	}

	public void GameOverShowPanel(){
		scoreText.SetActive (false);
		gameOverPanel.SetActive (true);

		finalScore.text = "Score \n" + ScoreManager.instance.GetScore ();//показываем финальный счет пользователю
		gameOverAnim.Play ("FadeIn");
	}

	void InitializeVariables(){// данной функцией мы задействуем наше меню счета в конце игры, активируем кнопки
		gameOverPanel = GameObject.Find ("Game Over Panel Holder");
		gameOverAnim = gameOverPanel.GetComponent<Animator> ();
		RestartBtn = GameObject.Find ("Restart").GetComponent<Button> ();
		BackBtn = GameObject.Find ("Back Button").GetComponent<Button> ();

		RestartBtn.onClick.AddListener (() => Restart ());
		BackBtn.onClick.AddListener (() => BackToMenu ());
		scoreText = GameObject.Find ("Score Text");
		finalScore = GameObject.Find ("Final Score").GetComponent<Text> ();

		gameOverPanel.SetActive (false);
	}

	public void Restart(){
		Application.LoadLevel (Application.loadedLevelName);
	}

	public void BackToMenu(){
		Application.LoadLevel ("MainMenu");
	}
}//GameOverManager
