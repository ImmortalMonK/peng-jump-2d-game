﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public static GameManager instance;
	[SerializeField]
	private GameObject player;

	[SerializeField]
	private GameObject platform;

	private float minX=-2.5f,maxX=2.5f,minY=-4.7f,maxY=-3.7f;
	//переменные, необходимые для создания новой платформы и движения камеры
	private bool lerpCamera;
	private float lerpTime = 1.5f;
	private float lerpX;

	void Awake(){
		MakeInstance ();
		CreateInitialPlatforms ();
	}

	void Update()
	{
		if (lerpCamera) {
			LerpTheCamera ();
		}
	}

	void MakeInstance(){
		if (instance == null)
			instance = this;
	}

	void CreateInitialPlatforms(){
		Vector3 temp = new Vector3 (Random.Range (minX, minX + 1.2f), Random.Range(minY, maxY), 0);
		// рандомное расположение платформ

		Instantiate (platform, temp, Quaternion.identity);// этот параметр создает клон объекта
		//используется платформа, расположение, вращение(сброс угла поворота)(поставит вращение в положение(0,0,0)
		temp.y += 2f;

		Instantiate (player, temp, Quaternion.identity);//то же , что и с платформами, только размещаем игрока

		temp = new Vector3 (Random.Range (maxX, maxX - 1.2f), Random.Range(minY, maxY), 0);

		Instantiate (platform, temp, Quaternion.identity);

	}//CreateInitialPlatforms

	void LerpTheCamera(){
		float x = Camera.main.transform.position.x;
		x = Mathf.Lerp (x, lerpX, lerpTime * Time.deltaTime);//время передвижения камеры
		// функция х показывает, с какого места в какое движется камера и какое время затрачивается на это

		Camera.main.transform.position=new Vector3(x, Camera.main.transform.position.y, Camera.main.transform.position.z);
		if (Camera.main.transform.position.x >= (lerpX - 0.07f)) {
			lerpCamera = false;
		}
	}

	public void CreateNewPlatformAndLerp(float lerpPosition){
		CreateNewPlatform ();
		lerpX = lerpPosition + maxX;
		lerpCamera = true;

	}

	void CreateNewPlatform(){
		float cameraX = Camera.main.transform.position.x;//даст нам положение по оси Х нашей камеры
		float newMaxX = (maxX * 2) + cameraX;//функция нужна для максимального удобного передвижения камеры
		Instantiate(platform, new Vector3(Random.Range(newMaxX,newMaxX - 1.2f),Random.Range(maxY,maxY - 1.2f),0), Quaternion.identity);
	}

}// GameManager












































