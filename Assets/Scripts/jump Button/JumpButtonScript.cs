﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class JumpButtonScript : MonoBehaviour, IPointerDownHandler,IPointerUpHandler//(интерфейсы, которые расскажут нам
//жмем ли мы на кнопку или нет)

{
	public void OnPointerDown(PointerEventData data)
	{
		if (PlayerJumpScript.instance != null) {
			PlayerJumpScript.instance.SetPower (true);
		}
	}
	public void OnPointerUp(PointerEventData data)
	{
		if (PlayerJumpScript.instance != null) {
			PlayerJumpScript.instance.SetPower (false);
		}
	}
}
